<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MonitorController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('api')->get('/monitors/{url}', [MonitorController::class, 'getMonitor'])->where('url', '.*');
Route::middleware('api')->post('/monitors', [MonitorController::class, 'addMonitor']);

Route::middleware('api')->get('/kek', function () {
    $queue = Queue::push('LogMessage',array('message'=>'Time: '.time()));
    return $queue;
});
class LogMessage{
    public function fire($job, $date){
        File::append(app_path().'/queue.txt',$date['message'].PHP_EOL);
        $job->delete();
    }
}
