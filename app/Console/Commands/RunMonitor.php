<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\MonitorWebPages;
use Illuminate\Foundation\Bus\DispatchesJobs;

class RunMonitor extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'monitor:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run web pages monitor job';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param MonitorWebPages $monitorWebPages
     * @return int
     */
    public function handle(MonitorWebPages $monitorWebPages)
    {
        $this->dispatch($monitorWebPages);
        return 0;
    }
}
