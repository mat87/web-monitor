<?php

namespace App\Service;
use App\Models\Measurement;
use App\Models\Url;

class WebPageInfo implements WebPageInfoInterface
{
    private int $timeOut = 30;
    private float $connectionTimeout = 0.5;

    /**
     * @param int $timeout
     */
    public function setTimeout(int $timeout): void
    {
        $this->timeOut = $timeout;
    }

    /**
     * @param float $cTimeout
     */
    public function setConnectionTimeout(float $cTimeout): void
    {
        $this->connectionTimeout = $cTimeout;
    }

    /**
     * @param string $url
     * @param int $redirectCount
     * @param float $timeCount
     * @return array
     */
    public function getInfo(string $url, int $redirectCount, float $timeCount): array
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->connectionTimeout);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeOut); //timeout in seconds

        curl_exec ($ch);
        $info = curl_getinfo($ch);

        $timeCount += $info['total_time'];

        if($info['http_code'] == '302' || $info['http_code'] == '302') {
            $redirectCount++;
            return $this->getInfo($info['redirect_url'], $redirectCount, $timeCount);
        }
        curl_close ($ch);

        return [$redirectCount, $timeCount];
    }

    /**
     * @param array $urlList
     * @return array
     */
    public function getStats(array $urlList): string
    {
        $stats = [];
        foreach($urlList as $url) {
            [$redirectCountResult, $timeCountResult] = $this->getInfo($url, 0, 0);

            if($timeCountResult >= $this->timeOut) {
                $timeCountResult = null;
            }

            $url = Url::firstOrCreate(['path' => $url]);
            $measurement = new Measurement;
            $measurement->url_id = $url->id;
            $measurement->redirects = $redirectCountResult;
            $measurement->download_time = $timeCountResult;
            $measurement->save();

            $stats[$url->path] = $timeCountResult;
        }

        return json_encode($stats);
    }
}
