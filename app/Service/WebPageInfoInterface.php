<?php

namespace App\Service;

interface WebPageInfoInterface
{
    public function getInfo(string $url, int $redirectCount, float $timeCount): array;

    public function getStats(array $urlList): string;
}
