<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Url;
use App\Service\WebPageInfo;
use Carbon\Carbon;

class MonitorController extends Controller
{
    const INTERVAL = 10;

    public function addMonitor(Request $request, WebPageInfo $webPageInfo)
    {
        $urlList = [];
        foreach ($request->post() as $url) {
            if($this->validateUrl($url)) {
                Url::firstOrCreate(['path' => $url]);
                $urlList[] = $url;
            } else {
                return response()->json([
                    'code' => 400,
                    'data' => (object)[],
                    'errors' => ['Invalid Url sent! ' . $url]
                ], 400);
            }
        }

        if (isset($request->status)) {
            $webPageInfo->setTimeout(2);
            $stats = $webPageInfo->getStats($urlList);

            return response()->json([
                'code' => 200,
                'data' => (object)[],
                'messages' => ['Urls saved!']
            ], 200)->header('X-Stats', $stats);
        }

        return response()->json([
            'code' => 200,
            'data' => (object)[],
            'messages' => ['Urls saved!']
        ], 200);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getMonitor(Request $request)
    {
        if(!$this->validateUrl($request->url)) {
            return response()->json([
                'code' => 400,
                'data' => (object)[],
                'errors' => ['Url invalid!']
            ], 400);
        }

        $date = Carbon::now('UTC')->subMinutes(self::INTERVAL)->format('Y-m-d H:i:s');

        $url = Url::firstWhere('path', $request->url);
        if(!$url) {
            return response()->json([
                'code' => 400,
                'data' => (object)[],
                'errors' => ['Url not found!']
            ], 400);
        }

        $measurements = $url->measurements()->where('created_at', '>=', $date)->get();
        $result =[];
        $message = '';

        foreach($measurements as $m) {
            $result[] = [$m->download_time, $m->created_at, $m->redirects];
        }

        if(!$result) {
            $message = 'No results for this url';
        }

        return response()->json([
            'code' => 200,
            'data' => (object)$result,
            'messages' => [$message]
        ], 200);
    }

    /**
     * @param string $url
     * @return bool
     */
    private function validateUrl(string $url): bool
    {
        if (preg_match('/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/', $url)) {
            return true;
        }
        return false;
    }
}
