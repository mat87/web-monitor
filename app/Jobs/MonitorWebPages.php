<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Service\WebPageInfo;
use App\Models\Url;

class MonitorWebPages implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $webPageInfo;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(WebPageInfo $webPageInfo)
    {
        $this->webPageInfo = $webPageInfo;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $urlList = Url::pluck('path')->toArray();
        $this->webPageInfo->getStats($urlList);
    }
}
